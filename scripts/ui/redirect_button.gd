extends Button

export(PackedScene) var REDIRECT_SCENE

func _pressed():
	get_tree().change_scene_to(REDIRECT_SCENE)
