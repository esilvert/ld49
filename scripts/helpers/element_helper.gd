class_name ElementHelper

static func max_id():
	var elements = all()
	var result = 0
	for element in elements:
		if int(element.ID) > result:
			result = int(element.ID)

	return result

static func count():
	return all().size()

static func all():
	return g_reactions_manager.get_tree().get_nodes_in_group('elements')

static func scale_for_id(id):
	var id_i = int(id)
	return log(id_i) / log(2)
