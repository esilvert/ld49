extends Node2D
class_name FSM

var current_state
var previous_state
var next_state
var state_duration

var STATES = []
export(NodePath) var root_path
onready var root = get_node(root_path)

func _ready():
	randomize()
	set_physics_process(true)


func set_states(states):
	STATES = states
	set_next_state(STATES[0])
	assert_state_definitions()

func set_next_state(state):
	next_state = state

func _physics_process(delta):
	if (next_state):
		update_fsm()

	state_method(delta)

	state_duration += delta

func update_fsm():
	previous_state = current_state
	current_state = next_state
	next_state = null
	state_duration = 0

	var on_enter_state_method = "on_state_%s" % current_state
	var on_exit_state_method = "on_state_%s_exited" % previous_state
	if (root.has_method(on_enter_state_method)):
		root.call(on_enter_state_method)

	if (root.has_method(on_exit_state_method)):
		root.call(on_exit_state_method)

	print ("FSM %s switched from %s to %s" % [name, previous_state, current_state])

func state_method(delta):
	var state_method = "state_%s" % current_state
	assert(root.has_method(state_method), "fsm %s has no method %s" % [name, state_method])
	root.call(state_method, delta)


func assert_state_definitions():
	for state in STATES:
		var state_method = "state_%s" % state
		assert(root.has_method(state_method), "fsm %s has no method %s" % [name, state_method])
