class_name ReactionsMananger
extends Node2D

func merge_for(ids):
	if ids_are_all_identical(ids):
		var sum = 0
		for id in ids:
			sum += int(id)
		return [sum]

	else:
		return []


func can_decompose(id):
	return int(id) > 1

func decompose_for(i):
	var possibles = []
	match i:
		1:
			return  []
		2:
			return [1, 1]
		3:
			possibles = [ [1, 2], [1, 1, 1]]
		4:
			possibles = [ [2, 2], [1, 3]]
		5:
			possibles = [ [2, 3, 1], [1,2,2], [1,1,1,2]]
		6:
			possibles = [  [2,2,2], [1, 2,3], [3, 3]]
		_:
			possibles = [ [i/2, i/2] ]
		# This implementation is a lot funnier,
		# but instantly kill a game because it is too hard. haha
		# _:
		# 	var half = i / 2
		# 	var rest = i - half
		# 	prints('half', half, 'rest', rest)
		# 	var temp = decompose_for(half)
		# 	prints('temp 1' ,temp)
		# 	temp.append_array(decompose_for(rest))
		# 	prints('temp 2', temp)
		# 	possibles = [temp]

	var rand_index = rand_range(0, possibles.size())
	return possibles[rand_index]

func ids_are_all_identical(ids):
	var first = int(ids[0])
	var result = true

	for id in ids:
		if int(id) != first:
			return false

	return result
