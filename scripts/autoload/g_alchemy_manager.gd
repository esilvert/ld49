class_name AlchemyManager
extends Node2D

var registered_collisions = []

signal new_reaction
signal decompose

func _ready():
	get_tree().connect('idle_frame', self, '_on_new_frame')

func clear_registered_collisions():
	registered_collisions.clear()

func register_collision(nodes):
	nodes.sort()

	# as a collision is detected by both parties, we need to register the collision only once
	if (registered_collisions.find(nodes) == -1):
		registered_collisions.append(nodes)

func decompose(element):
	emit_signal('decompose', element)

func _on_new_frame():
	#print("Alchemy found %d reactions to process" % registered_collisions.size())

	for reaction in registered_collisions:
		emit_signal('new_reaction', reaction)

	clear_registered_collisions()
