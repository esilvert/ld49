class_name ElementFactory
extends Node2D

const element_prefab = preload('res://scenes/prefabs/gameplay/element/element.tscn')

static func element_for_id(id):
	var element = element_prefab.instance()
	element.ID = id

	return element
