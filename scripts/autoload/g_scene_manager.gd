class_name SceneManager
extends Node2D

var main_menu
var win_menu
var lose_menu
var game
var ruler

signal pause;

onready var game_scene = preload('res://scenes/game.tscn')
onready var tutorial_scene = preload('res://scenes/level/tutorial.tscn')

func pause(active):
	get_tree().paused = active

func start_game():
	main_menu.hide()
	win_menu.hide()
	game.start()
	ruler.show()
	pause(false)

func restart_game():
	pause(true)
	get_tree().change_scene_to(game_scene)

func start_tutorial():
	pause(false)
	get_tree().change_scene_to(tutorial_scene)

func win_menu():
	pause(true)
	ruler.hide()
	win_menu.show()

func lose_menu():
	pause(true)
	ruler.hide()
	lose_menu.show()

func exit():
	get_tree().quit()
