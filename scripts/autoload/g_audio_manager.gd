class_name AudioManager

extends Node2D

const sounds = {
	'merge': [
		preload('res://assets/sounds/merge.wav'),
		preload('res://assets/sounds/merge_2.wav'),
		preload('res://assets/sounds/merge_3.wav'),
		],
	'decompose': [
		preload('res://assets/sounds/decompose.wav'),
		],
	}

const music = preload('res://assets/musics/ld49.ogg')

var sound_player
var music_player

func _ready():
	sound_player = AudioStreamPlayer.new()
	sound_player.volume_db = 5
	add_child(sound_player)

	music_player = AudioStreamPlayer.new()
	add_child(music_player)
	music_player.stream = music
	music.loop = true
	music_player.pause_mode = PAUSE_MODE_PROCESS
	music_player.play()

func play(sound):
	var library = sounds[sound]
	var sound_index = int(rand_range(0, library.size()))
	var sound_stream = library[sound_index]

	sound_player.stream = sound_stream
	sound_player.play()
