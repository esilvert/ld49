extends Control


export(int) var MAXIMUM_ATOMS = 20
export(int) var SCORE_TO_WIN = 256

onready var fsm = get_node('fsm')
onready var content = get_node('layer/content')
onready var progress = content.get_node('progress')
onready var limit = content.get_node('limit')

var has_won = false

var STATES = ['paused', 'running']

func _ready():
	g_scene_manager.ruler = self
	fsm.set_states(STATES)
	fsm.set_next_state('running')

func state_paused(delta):
	pass

func state_running(delta):
	var count = get_element_count()
	update_rules(count)

	handle_win_lose()

func get_element_count():
	return get_tree().get_nodes_in_group('elements').size()

func handle_win_lose():
	if (has_won == false && win_conditions()):
		has_won = true
		g_scene_manager.win_menu()
	elif (lose_conditions()):
		g_scene_manager.lose_menu()

func win_conditions():
	return ElementHelper.max_id() == SCORE_TO_WIN

func lose_conditions():
	return ElementHelper.count() >= MAXIMUM_ATOMS

func update_rules(count):
	var max_id = ElementHelper.max_id()
	var element_count = ElementHelper.count()

	var progress = log2(max_id) / log2(SCORE_TO_WIN)

	update_ui(progress, element_count)

func update_ui(progress_value, limit_value):
	progress.value = progress_value
	limit.value = limit_value
	limit.max_value = MAXIMUM_ATOMS

func log2(n):
	return log(n) / log(2.0)

func show():
	content.show()

func hide():
	content.hide()
