extends Camera2D

export(float) var SPEED = 100
export(float) var ZOOM_SPEED = 2
export(Vector2) var MIN_ZOOM = Vector2(0.3,0.3)
export(Vector2) var MAX_ZOOM = Vector2(2,2)
func _ready():
	set_physics_process(true)

func _physics_process(delta):
	handle_velocity(delta)
	handle_zoom(delta)

func handle_velocity(delta):
	var velocity = Vector2()
	if(Input.is_action_pressed('ui_right')):
		velocity.x += 1

	if(Input.is_action_pressed('ui_left')):
		velocity.x -= 1

	if(Input.is_action_pressed('ui_up')):
		velocity.y -= 1

	if(Input.is_action_pressed('ui_down')):
		velocity.y += 1

	velocity = velocity.normalized() * SPEED
	position += velocity * delta

func handle_zoom(delta):
	var camera_zoom = Vector2()
	if(Input.is_action_pressed('camera_unzoom')):
		camera_zoom.x += 1
		camera_zoom.y += 1

	if(Input.is_action_pressed('camera_zoom')):
		camera_zoom.x -= 1
		camera_zoom.y -= 1

	zoom += camera_zoom * ZOOM_SPEED * delta
	if(zoom.x < MIN_ZOOM.x):
		zoom.x = MIN_ZOOM.x
	if(zoom.y < MIN_ZOOM.y):
		zoom.y = MIN_ZOOM.y

	if(zoom.x > MAX_ZOOM.x):
		zoom.x = MAX_ZOOM.x
	if(zoom.y > MAX_ZOOM.y):
		zoom.y = MAX_ZOOM.y
