extends RigidBody2D

export(bool) var CAN_DECOMPOSE = true

export(float, 0, 100) var SPAWN_DURATION = 1

export(int) var BASE_SPEED = 100
export(float) var DECOMPOSING_TREMBLE = 10

var MIN_TRESHOLD = 10
var MAX_TRESHOLD = 30
var MIN_ESPERENCE = 2
var MAX_ESPERENCE = 10

export(NodePath) var fsm_path
onready var fsm = get_node(fsm_path)

onready var shape = get_node('shape')
onready var animator = get_node('animator')
onready var visual = get_node('visual')
onready var sprite = visual.get_node('sprite')
onready var label = visual.get_node('label')

onready var emitter_back = visual.get_node('emitter_back')
onready var emitter_front = visual.get_node('emitter_front')

export(String) var ID = "inert"

var velocity = Vector2()
var held = false


const STATES = ['spawn', 'idle', 'held', 'decomposing', 'delete']

func _ready():
	connect('mouse_entered', self, '_on_mouse_entered')
	connect('mouse_exited', self, '_on_mouse_exited')
	connect('input_event', self, '_on_input_event')
	connect('body_entered', self, '_on_body_entered')
	set_id(ID)

	fsm.set_states(STATES)

	name += "<%s>" % ID # debug
	label.text = String(ID)

func set_id(id):
	ID = id
	initialize_emitters()

# ------------------------------
# States
# ------------------------------

func _process(delta):
	initialize_scale()

func state_spawn(delta):
	if (fsm.state_duration > SPAWN_DURATION):
		fsm.set_next_state('idle')

func state_idle(delta):
	if can_decompose():
		var progress_to_decompose = pow(fsm.state_duration / decomposition_treshold(), 8)
		sprite.position = Vector2(rand_range(-1,1), rand_range(-1, 1)).normalized() * progress_to_decompose * DECOMPOSING_TREMBLE

		if progress_to_decompose > 1:
			fsm.set_next_state('decomposing')

func state_held(delta):
	global_transform.origin = get_global_mouse_position()

func state_decomposing(delta):
	var modulate = Color(randf(), randf(), randf())
	sprite.modulate = modulate

	if int(fsm.state_duration + delta) > int(fsm.state_duration): # Passes a second
		if rand_range(fsm.state_duration, decomposition_esperence()) < decomposition_esperence(): # random croissant oui oui
			register_for_decompose()

func state_delete(delta):
	if fsm.state_duration > 5:
		#queue_free()
		hide()
		set_process(false)
		set_physics_process(false)
		fsm.set_process(false)
		fsm.set_physics_process(false)
		# nique les segfault
		pass

func on_state_spawn():
	if velocity == Vector2.ZERO:
		pick_random_velocity()

func on_state_delete():
	visible = false
	shape.disabled = true

# ------------------------------
# Callbacks
# ------------------------------

# Only collide if has spawn & colliding with another elements
func _on_body_entered(body):
	if fsm.current_state != 'spawn' && body.is_in_group('elements'):
		g_alchemy_manager.register_collision([self, body])

func _on_mouse_entered():
	# if(fsm.current_state == 'idle'):
	# 	animator.play('hovered')
	pass

func _on_mouse_exited():
	# if(fsm.current_state == 'idle'):
	# 	animator.play('no_longer_hovered')
	pass

func _on_input_event(viewport, event, idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			pickup()
		elif event.button_index == BUTTON_RIGHT and event.pressed:
			register_for_decompose()

# ------------------------------
# Private
# ------------------------------

func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if is_held() && !event.pressed:
			drop(Input.get_last_mouse_speed())

func pickup():
	fsm.set_next_state('held')


func drop(impulse = Vector2.ZERO):
	apply_central_impulse(impulse)
	fsm.set_next_state('idle')

func is_held():
	return fsm.current_state == 'held'

func register_for_decompose():
	g_alchemy_manager.decompose(self)

func decompose():
	queue_free()

func pick_random_velocity():
	velocity = Vector2(rand_range(-1, 1), rand_range(-1, 1)).normalized() * BASE_SPEED
	linear_velocity = velocity

func decomposition_treshold():
	return clamp(MAX_TRESHOLD - int(ID), MIN_TRESHOLD, MAX_TRESHOLD)

func decomposition_esperence():
	return clamp(int(ID), MIN_ESPERENCE, MAX_ESPERENCE)

func can_decompose():
	return CAN_DECOMPOSE && g_reactions_manager.can_decompose(ID)

func mark_for_deletion():
	fsm.set_next_state('delete')
	remove_from_group('elements')

func initialize_emitters():
	var count = int(ID)
	var back_count = count / 3 * 2
	var front_count = count - back_count

	if back_count > 0:
		emitter_back.amount = back_count
		emitter_back.emitting = true
	else:
		emitter_back.emitting = false

	if front_count > 0:
		emitter_front.amount = front_count
		emitter_front.emitting = true
	else:
		emitter_front.emitting = false

func initialize_scale():
	var new_scale = clamp(ElementHelper.scale_for_id(ID),1, 10)
	visual.scale = Vector2(1,1) * new_scale

	var new_shape = shape.shape.duplicate()
	shape.shape = new_shape
	shape.shape.radius = 16 * new_scale

	mass = new_scale
