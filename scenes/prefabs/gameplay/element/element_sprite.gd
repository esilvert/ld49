extends Sprite


onready var element = get_parent().get_parent()

func _ready():
	var id = element.ID

	update_modulate(id)

func update_modulate(id):
	match id:
		'blue':
			modulate = Color.blue
		'red':
			modulate = Color.red
		'green':
			modulate = Color.green
		'yellow':
			modulate = Color.yellow
		'purple':
			modulate = Color.pink
