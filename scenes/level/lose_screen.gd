extends Control

export(bool) var AUTO_HIDE = true

func _ready():
	g_scene_manager.lose_menu = self
	if AUTO_HIDE:
		hide()

func _on_restart_pressed():
	g_scene_manager.restart_game()

func _on_exit_pressed():
	g_scene_manager.exit()

func hide():
	get_node('layer/content').hide()

func show():
	get_node('layer/content').show()
