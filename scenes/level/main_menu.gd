extends Control

export(NodePath) var game_path
onready var game = get_node(game_path)

func _ready():
	g_scene_manager.main_menu = self
	g_scene_manager.pause(true)

func _on_tutorial_pressed():
	g_scene_manager.start_tutorial()

func _on_game_pressed():
	g_scene_manager.start_game()

func hide():
	get_node('layer/content').hide()

func show():
	get_node('layer/content').show()
