extends Control

export(bool) var AUTO_HIDE = true

func _ready():
	g_scene_manager.win_menu = self
	if AUTO_HIDE:
		hide()

func _on_resume_pressed():
	g_scene_manager.start_game()

func _on_exit_pressed():
	g_scene_manager.exit()

func hide():
	get_node('layer/content').hide()

func show():
	get_node('layer/content').show()
