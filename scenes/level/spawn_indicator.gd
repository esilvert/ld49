extends TextureProgress

export(float) var MAX_SCALE = 2

func _process(delta):
	var progress= 1 + value / max_value
	set_scale( progress * Vector2(1, 1) * MAX_SCALE)
