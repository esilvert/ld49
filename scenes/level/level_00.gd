extends Node2D

onready var node_container = get_node('elements')
export(NodePath) var ruler_path
onready var ruler = get_node(ruler_path)

export(int) var ELEMENT_SIZE = 64
export(int) var SPAW_TIMESTAMP = 5
export(int) var PHASE_TWO_ID = 128

onready var progress = get_node('ui/root/spawn')

const MAX_ATOM_PER_ITERATION_LATE_GAME = 2
var max_atom_per_iteration = 4

func _ready():
	g_alchemy_manager.connect('new_reaction', self, '_on_new_reaction')
	g_alchemy_manager.connect('decompose', self, '_on_decompose')
	g_scene_manager.game = self

func start():
	ruler.fsm.set_next_state('running')
	$ui/root.show()

var process_duration = 0
var seconds_since_last_iteration = 0
func _process(delta):
	var sec = int(process_duration)
	process_duration += delta

	update_timer()

	if (sec < int(process_duration)): # second pass
		seconds_since_last_iteration += 1
		if seconds_since_last_iteration > SPAW_TIMESTAMP:
			spawn_new_atoms()
			seconds_since_last_iteration = 0
			process_duration = 0


func _on_new_reaction(elements):
	var ids = []
	var reference_element = elements[0]

	# Collect ids
	for element in elements:
		ids.append(element.ID)

	# Get merge ids for this combination
	var new_ids = g_reactions_manager.merge_for(ids)

	# Quick exit if reaction does not exist
	if new_ids == null:
		return

	if spawn_new_elements_around(new_ids, reference_element):
		for element in elements:
			element.mark_for_deletion()

func _on_decompose(element):
	var new_ids = g_reactions_manager.decompose_for(int(element.ID))

	if new_ids == null:
		return

	if spawn_new_elements_around(new_ids, element):
		decompose_element(element)

func spawn_new_elements_around(new_ids, element):
	var new_element_index = 0
	var new_elements_count = new_ids.size()

	var index = 0
	for id in new_ids:
		var factor = element.visual.scale.x * ELEMENT_SIZE

		var position = center()
		if index == 0:
			position += Vector2(factor, 0)
		else:
			position += Vector2(-factor, 0)

		spawn_element(id, position)
		index += 1

	if new_elements_count  > 0:
		g_audio_manager.play('merge')

	return new_ids

func center():
	return $center.position

func spawn_element(id, at):
		var new_element = ElementFactory.element_for_id(id)
		prints(new_element.name, 'before', new_element.scale)
		node_container.add_child(new_element)
		prints(new_element.name, 'after', new_element.scale)
		# randomize()
		new_element.position = at
		new_element.set_id(id)
		new_element.pick_random_velocity()

func decompose_element(element):
	element.decompose()
	g_audio_manager.play('decompose')

func spawn_new_atoms():
	var n = rand_range(2, max_atom_per_iteration)
	var i = 0
	while (i < n):
		spawn_new_atom()
		i += 1

func spawn_new_atom():
		var top_left = $top_left
		var bottom_right = $bottom_right

		var spawn = Vector2(
			rand_range(top_left.position.x, bottom_right.position.x),
			rand_range(top_left.position.y, bottom_right.position.y)
			)

		var elements = ElementHelper.all()

		var max_id = 0
		for element in elements:
			if int(element.ID)> max_id:
				max_id = int(element.ID)

		var possibles = [1, 2, 4]
		if max_id >= PHASE_TWO_ID:
			max_atom_per_iteration = MAX_ATOM_PER_ITERATION_LATE_GAME
			possibles = [2, 3, 4, 6, 8]

		var random_index = rand_range(0, possibles.size())

		var number = possibles[random_index]
		spawn_element(number, spawn)

func update_timer():
	progress.value = process_duration
	progress.max_value = SPAW_TIMESTAMP
