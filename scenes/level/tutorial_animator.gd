extends AnimationPlayer

export(NodePath) var title_path;
onready var title = get_node(title_path)

export(NodePath) var description_path;
onready var description = get_node(description_path)

export(NodePath) var cta_path;
onready var cta = get_node(cta_path)

export(NodePath) var level_path;
onready var level = get_node(level_path)
onready var content = title.get_parent()

var current_step = 0

func _ready():
	cta.connect('pressed', self, 'step')

func step():
	show_content()
	current_step += 1

	match current_step:
		1:
			step_1()
		2:
			step_2()
		3:
			step_3()
		4:
			step_4()

func step_1():
	title.text = "Core Mecanics"
	description.bbcode_text = "This game is all about merging \n"
	description.bbcode_text += "and decomposing elements.\n"
	description.bbcode_text += "An element is composed of a number of electrons that is written on it (see the 16 here ?)\n\n"

	description.bbcode_text += "You can decompose any element having more than 1 electron into smaller elements with half electrons.\n\n"

	description.bbcode_text += "To do so, right click it. Here, try on the 16."
	cta.text = "I have decomposed it"
	level.can_decompose = true

func step_2():
	title.text = "The Law"
	description.bbcode_text = "[b] Nothing is lost, nothing is created, everything is transformed[/b] \n\n"
	description.bbcode_text += "You can merge two identical atom into a bigger one with double electrons\n\n"
	description.bbcode_text += "To do so, click on an atom and drag it to another identical one\n\n"

	description.bbcode_text += "The purpose of this game is merge two 128 atoms into a 256\n\n"

	cta.text = "I have recreated the 16"
	level.can_merge = true

func step_3():
	title.text = "Unstable"
	description.bbcode_text = "Unfortunately, atoms during alchemy are quite unstable \n\n"
	description.bbcode_text += "They have a tendency to naturally decline into decomposition\n\n"
	description.bbcode_text += "Moreover, the chain reaction you caused generates a lot of free elements\n\n"

	description.bbcode_text += "Here, play a bit with these new atoms that just appeared\n\n"

	cta.text = "I am ready to play"
	level.spawn_new_atoms(8, 10)
	level.spawn_element(6)
	level.spawn_element(5)
	level.spawn_element(3)

func step_4():
	g_scene_manager.restart_game()

func _on_tutorial_visibility_toggle():
	if content.is_visible_in_tree():
		content.hide()
	else:
		content.show()

func show_content():
	content.show()
