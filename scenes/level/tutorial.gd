extends Node2D

onready var node_container = get_node('elements')

export(int) var ELEMENT_SIZE = 64
export(int) var SPAW_TIMESTAMP = 5
export(int) var PHASE_TWO_ID = 128

var can_decompose = false
var can_merge = false

const MAX_ATOM_PER_ITERATION_LATE_GAME = 2
var max_atom_per_iteration = 4

func _ready():
	g_alchemy_manager.connect('new_reaction', self, '_on_new_reaction')
	g_alchemy_manager.connect('decompose', self, '_on_decompose')
	set_process(false)

func start():
	$layer/content.show()
	$layer/toggle_tutorial.show()
	pass

var process_duration = 0
var seconds_since_last_iteration = 0
func _process(delta):
	var sec = int(process_duration)
	process_duration += delta

	if (sec < int(process_duration)): # second pass
		seconds_since_last_iteration += 1
		if seconds_since_last_iteration > SPAW_TIMESTAMP:
			spawn_new_atoms()
			seconds_since_last_iteration = 0


func _on_new_reaction(elements):
	if !can_merge:
		return

	var ids = []
	var reference_element = elements[0]

	# Collect ids
	for element in elements:
		ids.append(element.ID)

	# Get merge ids for this combination
	var new_ids = g_reactions_manager.merge_for(ids)

	# Quick exit if reaction does not exist
	if new_ids == null:
		return

	if spawn_new_elements_around(new_ids, reference_element):
		for element in elements:
			element.mark_for_deletion()

func _on_decompose(element):
	if !can_decompose:
		return

	var new_ids = g_reactions_manager.decompose_for(int(element.ID))

	if new_ids == null:
		return

	if spawn_new_elements_around(new_ids, element):
		decompose_element(element)

func spawn_new_elements_around(new_ids, element):
	var new_element_index = 0
	var new_elements_count = new_ids.size()

	for id in new_ids:
		var angle = new_element_index / new_elements_count * 2 * PI
		spawn_element(id, element.position + Vector2(1, 0).rotated(angle) * ELEMENT_SIZE)

	if new_elements_count  > 0:
		g_audio_manager.play('merge')

	return new_ids

func spawn_element(id, at = null):
	if !at:
		at = $top_left.position

	var new_element = ElementFactory.element_for_id(id)
	node_container.add_child(new_element)
	# randomize()
	new_element.position = at
	new_element.pick_random_velocity()

func decompose_element(element):
	element.decompose()
	g_audio_manager.play('decompose')

func spawn_new_atoms(lower_bound = 2, upper_bound = null):
	if !upper_bound:
		upper_bound = max_atom_per_iteration

	var n = rand_range(lower_bound, upper_bound)
	var i = 0
	while (i < n):
		spawn_new_atom()
		i += 1

func spawn_new_atom():
	var top_left = $top_left
	var bottom_right = $bottom_right

	var spawn = Vector2(
		rand_range(top_left.position.x, bottom_right.position.x),
		rand_range(top_left.position.y, bottom_right.position.y)
		)

	var elements = ElementHelper.all()

	var max_id = 0
	for element in elements:
		if int(element.ID)> max_id:
			max_id = int(element.ID)

	var possibles = [1, 2, 4]
	if max_id >= PHASE_TWO_ID:
		max_atom_per_iteration = MAX_ATOM_PER_ITERATION_LATE_GAME
		possibles = [2, 3, 4, 6, 8]

	var random_index = rand_range(0, possibles.size())

	var number = possibles[random_index]
	spawn_element(number, spawn)

func enable_merge():
	can_merge = true

func enable_decompose():
	can_decompose = true
